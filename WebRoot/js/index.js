
// 导航栏
$('nav').children('div').children('ul').children('li').mouseenter(function () {
    $('nav').children('div').children('ul').children('li').removeClass('nav_choose');
    $(this).addClass('nav_choose');
});
$('nav').mouseleave(function () {
    $('nav').children('div').children('ul').children('li').removeClass('nav_choose');
    $('#gkk').addClass('nav_choose');
});

// 导航栏下拉菜单
$('nav .nav_down').mouseenter(function () {
    $('#nav_down').stop().slideDown();
    $(this).children('img').attr('src', 'img/nav_down2.png');
});
$('nav .nav_down').mouseleave(function () {
    $('#nav_down').stop().slideUp();
    $(this).children('img').attr('src', 'img/nav_down.png');
});

$(window).scroll(function () {
    if ($(this).scrollTop() > 300) {
        $('#top').fadeIn();
        $('body').css('paddingTop', '90px');
        $('nav').css({
            'position': 'fixed',
            'top': '0',
            'left': '0',
            'border-bottom': '1px solid #e6e6e6'
        });
    }
    else {
        $('body').css('paddingTop', '0');
        $('nav').css({
            'position': 'initial',
            'top': '0',
            'left': '0',
            'border-bottom': 'none'
        });
    }
});
// 登录注册显示和消失弹出框
$('#reg_open').click(function () {
    $('#reg').removeClass('hidden');
});
$('#login_open').click(function () {
    $('#login').removeClass('hidden');
});
$('#reg_close').click(function(){
    $('#reg').addClass('hidden');
});
$('#login_close').click(function(){
    $('#login').addClass('hidden');
});


// 报名
$("#iform .button").click(function (ev) {
    ev.preventDefault();

    var input1 = $('.form-control:eq(0)').val();
    var input2 = $('.form-control:eq(1)').val();
    var input3 = $('.form-control:eq(2)').val();
    var reg2 = /^1[3578]\d{9}$/;
    var reg3 = /^[1-9]\d{4,10}$/;

    if (input1 == "") {
        alert("姓名不能为空");
    } else if (!reg2.test(input2)) {
        alert("手机格式不符！！请重新填写！");
    } else if (!reg3.test(input3)) {
        alert("QQ格式不符！！请重新填写！");
    } else {
        $("#iform").submit();
    }

});


// 获取url路径中的参数
function GetUrlParam(paraName) {
    var url = document.location.toString();
    var arrObj = url.split("?");

    if (arrObj.length > 1) {
        var arrPara = arrObj[1].split("&");
        var arr;
        for (var i = 0; i < arrPara.length; i++) {
            arr = arrPara[i].split("=");

            if (arr != null && arr[0] == paraName) {
                return arr[1];
            }
        }
        return "";
    }
    else {
        return "";
    }
}




//==========================Java03班升级JS===============================


var verifyCode = new GVerify("v_container");


var regEmail = false;
var regPsw = false;
var regYzm = false;


$('#regEmail').blur(function(){
	var emailVal = $(this).val();
	if(null != emailVal && '' != emailVal){
		var params = {'email':emailVal}
		$.post('user/validateEmail',params,function(data){
			if(data == 'success'){
				regEmail = true;
				$('#emailMsg').text('该邮箱可用!').css({'color':'green'});
			}else{
				regEmail = false;
				$('#emailMsg').text('该邮箱不可用!').css({'color':'red'});
			}
			
		});
	}
});

$('#regPsw').blur(function(){
	var pswVal = $(this).val();
	var pswAgainVal = $('#regPswAgain').val();
	if(null == pswVal || '' == pswVal){
		regPsw = false;
		$('#passMsg').text('密码不能为空').css({'color':'red'})
	}else if(pswVal != pswAgainVal){
		regPsw = false;
		$('#passMsg').text('两次密码不一致，请重新输入').css({'color':'red'})
	}else{
		regPsw = true;
		$('#passMsg').text('')
	}
});

$('#regPswAgain').blur(function(){
	var pswVal = $('#regPsw').val();
	var pswAgainVal = $(this).val();
	if(null == pswVal || '' == pswVal){
		regPsw = false;
		$('#passMsg').text('密码不能为空').css({'color':'red'})
	}else if(pswVal != pswAgainVal){
		regPsw = false;
		$('#passMsg').text('两次密码不一致，请重新输入').css({'color':'red'})
	}else{
		regPsw = true;
		$('#passMsg').text('')
	}
})

$('input[name="yzm"]').blur(function(){
	var code = $(this).val();
	if(verifyCode.validate(code)){
		regYzm = true;
	}else{
		regYzm = false;
	}
})

$('input[name="yzm"]').focus(function(){
	$('#yzmMsg').text('');
});
//var code = $('input[name="yzm"]')
//verifyCode.validate(code) //如果验证码正确，返回true


function commitRegForm(){
	if(regEmail && regPsw && regYzm){
		//注册信息校验通过， ajax方式提交表单
		$.ajax({
			url: 'user/insertUser',		//请求地址
			type: 'POST',		//请求类型
			data: $('#regForm').serialize(),		//参数  表单序列化，代替参数的键值对
			success:function(data){
				$('#reg').addClass('hidden'); //隐藏注册界面
			},
			error:function(){
				alert("联系管理员")
			}
		});
	}else{
		$('#yzmMsg').text('验证码有误').css({'color':'red'});
	}
}


//ajax 验证登录邮箱  不能为空且存在
$('#loginEmail').blur(function(){
	var loginVal = $(this).val();
	if(null != loginVal && '' != loginVal){
		//验证存在
		var params = {'email':loginVal}
		$.post('user/validateLoginEmail',params,function(data){
			if(data=='success'){
				$('#loginEmailMsg').text('')
			}
			else{
				$('#loginEmailMsg').text('该邮箱不存在').css({'color':'red'})
			}
		});
	}else{
		$('#loginEmailMsg').text('请输入邮箱').css({'color':'red'})
	}
})

//点击登录时，ajax验证密码
function commitLogin(){
	var loginEmailVal = $('#loginEmail').val();
	var loginPswVal = $('#loginPassword').val();
	var params={'email':loginEmailVal,'password':loginPswVal}
	$.post('user/validateLogin',params,function(data){
		if(data=='success'){
			$('#login').addClass('hidden');
		}else{
			alert('密码不正确，请重新输入')
		}
	});
}



