<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="zh-CN">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <!-- Bootstrap -->
      <link href="${pageContext.request.contextPath }/css/bootstrap.min.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://cdn.bootcss.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
      <script src="https://cdn.bootcss.com/jquery/1.12.4/jquery.min.js"></script>
      <script src="${pageContext.request.contextPath }/js/bootstrap.min.js"></script>
    <style type="text/css">
    	.box{
    		width: 260px;
    		margin: 200px auto;
    	}
    </style>
  </head>
  
  <body>
		<div class="box">
			<form action="login" method="post">
				<img src="${pageContext.request.contextPath }/img/header_logo.png" class="img-responsive center-block" alt="Responsive image">
				<input type="text" name="username" class="form-control" placeholder="用户名" style="margin: 10px auto;border-color: green;">
				<input type="password" name="password" class="form-control" placeholder="密码" style="margin-bottom: 30px;border-color: green;">
				<button type="submit" class="btn btn-success btn-block">登录</button>
			</form>
		</div>
    
  </body>
</html>