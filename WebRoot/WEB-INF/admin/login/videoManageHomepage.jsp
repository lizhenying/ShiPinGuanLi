<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="zh-CN">

	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
		<title>Bootstrap 101 Template</title>

		<!-- Bootstrap -->
		<link href="${pageContext.request.contextPath }/css/bootstrap.min.css" rel="stylesheet">

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
      <script src="https://cdn.bootcss.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
      		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="https://cdn.bootcss.com/jquery/1.12.4/jquery.min.js"></script>
		<script src="${pageContext.request.contextPath }/js/bootstrap.min.js"></script>
    
    	
		<style type="text/css">
			tr{
				border: 2px solid lightgray;
			}
			
			th {
				text-align: center;
			}
			.navbar-brand{
				color: white;
			}
		</style>
	</head>

	<jsp:include page="/WEB-INF/admin/head.jsp"></jsp:include>	
	<body>
		<div class="jumbotron" style="padding-top: 15px;padding-bottom: 15px;">
			<div class="container">
				<h2>视频管理系统首页</h2>
			</div>
		</div>
		<div>
			欢迎回来
		</div>
		
	</body>

</html>