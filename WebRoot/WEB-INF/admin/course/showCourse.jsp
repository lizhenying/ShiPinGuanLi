<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="zh-CN">

	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
		<title>Bootstrap 101 Template</title>

		<!-- Bootstrap -->
		<link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
	      <script src="https://cdn.bootcss.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	      <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
	    <![endif]-->
	    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
			<script src="https://cdn.bootcss.com/jquery/1.12.4/jquery.min.js"></script>
			<!-- Include all compiled plugins (below), or include individual files as needed -->
			<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
		<style type="text/css">
			tr {
				border: 2px solid lightgray;
			}
			
			th {
				text-align: center;
			}
		</style>
	</head>

	<body>
		<jsp:include page="/WEB-INF/admin/head.jsp"></jsp:include>

		<div class="jumbotron" style="padding-top: 15px;padding-bottom: 15px;">
			<div class="container">
				<h2>课程管理</h2>
			</div>
		</div>

		<div class="container">
			<a href="addOrEditCourse?id=0"><button type="button" class="btn btn-info">添加</button></a>
			<div class="container" style="margin-top:20px;">
				<table class="table table-bordered table-hover" style="text-align:center;">
					<thead>
						<tr class="active">
							<th><input type="checkbox"/></th>
							<th>序号</th>
							<th>主讲人</th>
							<th width="15%">标题</th>
							<th width="50%">简介</th>
							<th>编辑</th>
							<th>删除</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${courseList }" var="course" varStatus="varC">
							<tr>
								<th><input type="checkbox"/></th>
								<td>${varC.count }</td>
								<td>主讲人</td>
								<td>${course.courseTitle }</td>
								<td>${course.courseDesc }</td>
								<td>
									<a href="addOrEditCourse?id=${course.id }"><span class="glyphicon glyphicon-edit"></span></a>
								</td>
								<td>
									<a href="deleteCourse"><span class="glyphicon glyphicon-trash"></span></a>
								</td>
							</tr>
						</c:forEach>
						
					</tbody>
				</table>
			</div>
		</div>
	</body>

</html>