<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="zh-CN">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="${pageContext.request.contextPath }/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://cdn.bootcss.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://cdn.bootcss.com/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="${pageContext.request.contextPath }/js/bootstrap.min.js"></script>
    <style type="text/css">
       th{
          text-align:center;
       }
    </style>
  </head>
  <body>
  
   
	       <nav class="navbar-inverse">
		      <div class="container">
		        <!-- Brand and toggle get grouped for better mobile display -->
		        <div class="navbar-header">
		          
		          <a class="navbar-brand" href="#">视频管理系统</a>
		        </div>
		
		        
		        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-9">
		          <ul class="nav navbar-nav">
		          		<li>
							<a href="showVideo.action">视频管理</a>
						</li>
						<li>
							<a href="mainTeacher.action">主讲人</a>
						</li>
						<li class="active">
							<a href="showCourse.action">课程管理</a>
						</li>
		          </ul>
		           <p class="navbar-text navbar-right">
		            <span>admin</span>&nbsp;<i class="glyphicon glyphicon-log-in"></i>&nbsp;&nbsp;<a href="#" class="navbar-link">退出</a>
		           </p>
		        </div>
		        
		       
		      </div>
		    </nav>
		    
		    <div class="jumbotron" style="padding-top: 15px;padding-bottom: 15px;">
		       <div class="container" >
		           <h2>添加课程</h2>
		       </div>
			</div>
	  	
	  	
	  	<div class="container">
	  		

<form class="form-horizontal" action="showCourse.action" method="post">
	<div class="form-group">
		<label for="inputEmail3" class="col-sm-2 control-label">所属学科</label>
		<div class="col-sm-10">
		<select class="form-control">
			<option>选择所属学科</option>
			<option>1</option>
			<option>2</option>
			<option>3</option>
			<option>4</option>
			<option>5</option>
		</select>
		</div>
	</div>
	<div class="form-group">
		<label for="inputPassword3" class="col-sm-2 control-label">标题</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="inputPassword3" placeholder="课程标题">
			</div>
	</div>
	<div class="form-group">
		<label for="inputPassword3" class="col-sm-2 control-label">简介</label>
		<div class="col-sm-10">
		<textarea class="form-control" rows="3"></textarea>
		</div>
	</div>
	
	<div class="form-group">
		<span class="col-sm-2"></span>
		<div class="col-sm-10">
		<input class="btn btn-default" type="submit" value="保存">
		</div>
	</div>
</form>
	
	</div>

  </body>
</html>