<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="zh-CN">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
     <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://cdn.bootcss.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	  <script src="http://static.runoob.com/assets/jquery-validation-1.14.0/dist/jquery.validate.min.js"></script>
     <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
   <!--  <script src="https://cdn.bootcss.com/jquery/1.12.4/jquery.min.js"></script> -->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/messages_zh.js"></script>
    <style type="text/css">
       th{
          text-align:center;
       }
       	
    </style>
  </head>
  <body>
	       <jsp:include page="/WEB-INF/admin/head.jsp"></jsp:include>
		    <div class="jumbotron" style="padding-top: 15px;padding-bottom: 15px;">
		       <div class="container" >
		           <h2>编辑课程信息</h2>
		       </div>
			</div>
	  	
	  	
	  	<div class="container">
	  		

<form class="form-horizontal" action="addOrEditCourse" method="post" id="formId">
	<div class="form-group">
		<label for="inputPassword3" class="col-sm-2 control-label">课程名称</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="inputPassword3" 
				placeholder="视频名称" value="${video.title }" name="title" >
				<input type="hidden" value="${video.id }" name="id">
			</div>
	</div>
	
	<div class="form-group">
		<label for="inputEmail3" class="col-sm-2 control-label">主讲人</label>
		<div class="col-sm-10">
		<select class="form-control" name="spearkerId" >
			<option value="0">选择主讲人</option>
				<c:forEach items="${speakerList }" var="speaker">
					<option value="${speaker.id }"
						<c:if test="${speaker.id == video.spearkerId }">selected</c:if>
					>${speaker.speakerName }</option>
				</c:forEach>
		</select>
		</div>
	</div>
	
	<div class="form-group">
		<label for="inputPassword3" class="col-sm-2 control-label">课程描述</label>
		<div class="col-sm-10">
		<textarea class="form-control" rows="3" name="detail" >${video.detail }</textarea>
		</div>
	</div>
	
	<div class="form-group">
		<span class="col-sm-2"></span>
		<div class="col-sm-10">
		<input class="btn btn-default" type="submit" value="保存">
		</div>
	</div>
	
</form>
	
	</div>

  </body>
</html>