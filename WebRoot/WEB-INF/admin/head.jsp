<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="zh-CN">

	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
		<title>Bootstrap 101 Template</title>

		<!-- Bootstrap -->
		<%-- <link href="${pageContext.request.contextPath }/css/bootstrap.min.css" rel="stylesheet"> --%>

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
      <script src="https://cdn.bootcss.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
		<style type="text/css">
			tr{
				border: 2px solid lightgray;
			}
			
			th {
				text-align: center;
			}
			.navbar-brand{
				color: white;
			}
		</style>
	</head>

	<body>
		<nav class="navbar-inverse">
			<div class="container">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<a class="navbar-brand" 
					href="${pageContext.request.contextPath }/admin/videoManageHomepage">视频管理系统</a>
				</div>

				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-9">
					<ul class="nav navbar-nav">
						<li>
							<a href="${pageContext.request.contextPath }/video/showVideo">视频管理</a>
						</li>
						<li>
							<a href="${pageContext.request.contextPath }/speaker/showSpeaker">主讲人管理</a>
						</li>
						<li>
							<a href="${pageContext.request.contextPath }/course/showCourse">课程管理</a>
						</li>
					</ul>
					<p class="navbar-text navbar-right">
						<span>欢迎你，${sessionScope.username }</span>&nbsp;<span class="glyphicon glyphicon-log-in"></span>&nbsp;&nbsp;
						<a href="loginOut" class="navbar-link">退出</a>
					</p>
				</div>
			</div>
		</nav>
	</body>

</html>