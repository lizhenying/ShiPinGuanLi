<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="zh-CN">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
     <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://cdn.bootcss.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
     <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
   	<script src="https://cdn.bootcss.com/jquery/1.12.4/jquery.min.js"></script>
	  <script src="http://static.runoob.com/assets/jquery-validation-1.14.0/dist/jquery.validate.min.js"></script>
    <![endif]-->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/messages_zh.js"></script>
    <style type="text/css">
       th{
          text-align:center;
       }
       	input.error { border: 1px solid red; }
		label.error {
		  background:url("${pageContext.request.contextPath}/img/unchecked.gif") no-repeat 0px 0px;
		
		  padding-left: 16px;
		
		  padding-bottom: 2px;
		
		  font-weight: bold;
		
		  color: #EA5200;
		}
		label.checked {
		  background:url("img/checked.gif") no-repeat 0px 0px;
		}
    </style>
  </head>
  <body>
	       <jsp:include page="/WEB-INF/admin/head.jsp"></jsp:include>
		    <div class="jumbotron" style="padding-top: 15px;padding-bottom: 15px;">
		       <div class="container" >
		           <h2>编辑视频信息</h2>
		       </div>
			</div>
	  	
	  	
	  	<div class="container">
	  		

<form class="form-horizontal" action="addOrEditVideo" method="post" id="formId">
	<div class="form-group">
		<label for="inputPassword3" class="col-sm-2 control-label">视频标题</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="inputPassword3" 
				placeholder="视频名称" value="${video.title }" name="title" required>
				<input type="hidden" value="${video.id }" name="id">
			</div>
	</div>
	
	<div class="form-group">
		<label for="inputEmail3" class="col-sm-2 control-label">主讲人</label>
		<div class="col-sm-10">
		<select class="form-control" name="spearkerId" required>
			<option value="">选择主讲人</option>
				<c:forEach items="${speakerList }" var="speaker">
					<option value="${speaker.id }"
						<c:if test="${speaker.id == video.spearkerId }">selected</c:if>
					>${speaker.speakerName }</option>
				</c:forEach>
		</select>
		</div>
	</div>
	
	<div class="form-group">
		<label for="inputEmail3" class="col-sm-2 control-label">所属课程</label>
		<div class="col-sm-10">
		<select class="form-control" name="courseId" required>
			<option value="">选择所属课程</option>
			<c:forEach items="${courseList }" var="course">
				<option value="${course.id }"
					<c:if test="${course.id==video.courseId }">selected</c:if>
				>${course.courseTitle }
				</option>
			</c:forEach>
		</select>
		</div>
	</div>
	<div class="form-group">
		<label for="inputPassword3" class="col-sm-2 control-label">视频时长</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="inputPassword3" 
				placeholder="精确到秒" value="${video.time }" name="time" required>
			</div>
	</div>
	
	<div class="form-group">
		<label for="inputPassword3" class="col-sm-2 control-label">封面图片地址</label>
			<div class="col-sm-10">
				<input type="url" class="form-control" id="inputPassword3" 
				placeholder="具体url" value="${video.imageUrl }" name="imageUrl" required>
			</div>
	</div>
	
	<div class="form-group">
		<label for="inputPassword3" class="col-sm-2 control-label">视频播放地址</label>
			<div class="col-sm-10">
				<input type="url" class="form-control" id="inputPassword3" 
				placeholder="具体url" value="${video.videoUrl }" name="videoUrl" required>
			</div>
	</div>
	
	<div class="form-group">
		<label for="inputPassword3" class="col-sm-2 control-label">备注</label>
		<div class="col-sm-10">
		<textarea class="form-control" rows="3" name="detail" required>${video.detail }</textarea>
		</div>
	</div>
	
	<div class="form-group">
		<span class="col-sm-2"></span>
		<div class="col-sm-10">
		<input class="btn btn-default" type="submit" value="保存">
		</div>
	</div>
	
</form>
	
	</div>
	<script type="text/javascript">
		$().ready(function() {
		    $("#formId").validate();
		});
	</script>
  </body>
</html>