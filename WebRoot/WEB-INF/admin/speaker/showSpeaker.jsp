<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="zh-CN">

	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
		<title>Bootstrap 101 Template</title>

		<!-- Bootstrap -->
		<link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
      <script src="https://cdn.bootcss.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
   		 <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
			<script src="https://cdn.bootcss.com/jquery/1.12.4/jquery.min.js"></script>
			<!-- Include all compiled plugins (below), or include individual files as needed -->
			<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
			<script src="${pageContext.request.contextPath}/js/confirm.js"></script>
		<style type="text/css">
			tr {
				border: 2px solid lightgray;
			}
			
			th {
				text-align: center;
			}
		</style>
	</head>

	<body>
		<jsp:include page="/WEB-INF/admin/head.jsp"></jsp:include>
		<div class="jumbotron" style="padding-top: 15px;padding-bottom: 15px;">
			<div class="container">
				<h2>主讲人管理</h2>
			</div>
		</div>

		<div class="container">
			<a href="addOrEditSpeaker/0"><button type="button" class="btn btn-info">添加</button></a>
			<div class="container" style="margin-top:20px;">
				<table class="table table-bordered table-hover" style="text-align:center;">
					<thead>
						
							<tr class="active">
								<th><input type="checkbox"/></th>
								<th>序号</th>
								<th>名称</th>
								<th>职位</th>
								<th width="50%">简介</th>
								<th>编辑</th>
								<th>删除</th>
							</tr>
					</thead>
					<tbody>
						<c:forEach items="${speakerList }" var="speaker" varStatus="varS">
							<tr>
								<td><input type="checkbox"/></td>
								<td>${varS.count }</td>
								<td>${speaker.speakerName }</td>
								<td>${speaker.speakerJob }</td>
								<td>${speaker.speakerDesc }</td>
								<td>
									<a href="addOrEditSpeaker/${speaker.id }"><span class="glyphicon glyphicon-edit"></span></a>
								</td>
								<td>
									<a href="#" onclick="showConfirm(this,'${speaker.speakerName}',${speaker.id })">
									<span class="glyphicon glyphicon-trash"></span>
									</a>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
			
		<script type="text/javascript">
			$(function(){
			});
			
			//删除主讲人
			function showConfirm(obj,name,id){
				 Confirm.show('温馨提示', '您确定要删除"'+name+'"吗？', {
                '确定': {
                    'primary': true,
                    'callback': function() {
                        //使用ajax删除数据
                        var params={"id":id};
                        $.post("deleteSpeaker.action",params,function(data){
                            if(data=='success'){
                               //通过js删除tr节点
                               $(obj).parent().parent().remove();//$(obj) 获取的是a,$(obj).parent() 获取td
                               Confirm.show('温馨提示', '删除成功！');
                            }else{
                               Confirm.show('温馨提示', '删除失败！');
                            }
                        });
                    }
                }
            });
			}
		</script>
	</body>

</html>