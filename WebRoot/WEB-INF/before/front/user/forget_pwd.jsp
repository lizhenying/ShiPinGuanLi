<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <base href="${pageContext.request.contextPath}/">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta charset="utf-8">
    <meta name="renderer" content="webkit">
    <meta name="keywords" content="Web前端视频教程,大数据视频教程,HTML5视频教程,UI视频教程,PHP视频教程,java视频教程,python基础教程">
    <meta name="description" content="一根筋教育在线课程视频,为您提供java,python,HTML5,UI,PHP,大数据等学科经典视频教程在线浏览学习,精细化知识点解析,深入浅出,想学不会都难,一根筋教育,学习成就梦想！">
    <link rel="stylesheet" href="css/base.css">
    <link rel="stylesheet" href="css/forget_password.css">
    <link rel="icon" href="favicon.png" type="image/png">
    <title>在线公开课-一根筋教育|java|大数据|HTML5|python|UI|PHP视频教程</title>

</head>

<body>
    <header>
        <div class="container">
            <img src="img/header_logo.png" alt="一根筋">
        </div>
    </header>
    <main>
        <div class="container">
            <form class="ma" action="user/validateEmailCode">
                <div class="form_header">
                    <div class="form_title">
                        <h2>忘记密码</h2>
                        <span>通过注册邮箱重设密码</span>
                    </div>
                    <div class="form_back">
                        <a href="index.jsp">返回立即登录</a>
                    </div>
                </div>
                <div class="form_body">
                    <input type="email" placeholder="请输入登录邮箱" name="email">
                    <input type="text" placeholder="请输入验证码" name="code"><input type="button" id="yzmBtn" value="发邮件获取验证码"/>
                    <span style="color:red;">${errorMsg }</span>
                    <input type="submit" value="提交" onclick="return commitForm()";>
                </div>
                <div class="form_footer">
                    <div class="FAQ">
                        <span>收不到邮件？查看</span><a href="#">常见问题</a>
                    </div>
                </div>
            </form>
        </div>
    </main>
<%@include file="../include/script.html"%>
    <script type="text/javascript">
		$('#yzmBtn').click(function(){
			var email = $('input[name="email"]').val();
			if(null != email && email != ''){
				//ajax
				var params = {'email':email};
				$.post('user/sendEmail',params,function(data){
					if(data == 'success'){
						alert('验证码发送成功，请注意查收')
					}else{
						alert('验证码发送失败')
					}
				})
				//异步  倒计时
				var time = 5;
				$('#yzmBtn').attr('disabled','disabled').css('backgroundColor','gray');
				$('#yzmBtn').val(time+'s倒计时');
				var timer = setInterval(function(){
					time --;
					$('#yzmBtn').val(time+'s倒计时');
					if(time == 0){
						clearInterval(timer);
						$('#yzmBtn').val('发邮件获取验证码');
						$('#yzmBtn').removeAttr('disabled').css('backgroundColor','#576282');
					}
				}, 1000);
			}else{
				alert('请输入邮箱！')
			}
		});
		
		
		
		
    </script>
</body>

</html>