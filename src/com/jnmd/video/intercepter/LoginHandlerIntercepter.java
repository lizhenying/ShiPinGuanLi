package com.jnmd.video.intercepter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

public class LoginHandlerIntercepter implements HandlerInterceptor {
	//controller方法执行之后，并且视图返回之后执行
	@Override
	public void afterCompletion(HttpServletRequest arg0,
			HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {
		// TODO Auto-generated method stub

	}
	//controller方法执行之后，并且视图未返回
	@Override
	public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1,
			Object arg2, ModelAndView arg3) throws Exception {
		// TODO Auto-generated method stub

	}
	//controller方法执行之前
	//return true  放行
	//return false 表示拦截住，不放行
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response,
			Object arg2) throws Exception {
		System.out.println("---------------preHandle-----------------");
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		Object username = session.getAttribute("username");
		if(null != username){
			return true;
		}else{
			System.out.println("intercepter showVideo.action");
			response.sendRedirect("showLogin");
			return false;
		}
	}

}
