package com.jnmd.video.utils;

import org.springframework.util.DigestUtils;

public class Md5Utils {
	public static String md5(String str){
		//一次加密
		String pass = DigestUtils.md5DigestAsHex(str.getBytes());
		pass = DigestUtils.md5DigestAsHex((pass + pass.substring(pass.length() - 6)).getBytes());
		return pass;
	}
}
