package com.jnmd.video.utils;

public class TimeUtils {
	public static String getFormatTime(int time){
		int hour = time / 3600;
		int minute = time / 60;
		int second = time % 60;
		return formatTime(hour)+":"+formatTime(minute)+":"+formatTime(second);
	}
	
	public static String formatTime(int t){
		if(t < 10){
			return "0" + t;
		}else{
			return "" + t;
		}
	}
}
