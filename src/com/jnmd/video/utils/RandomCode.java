package com.jnmd.video.utils;


public class RandomCode {
	//随机验证码
	public static String getRandomCode(int length){
		String codeStr = "";
		for (int i = 0; i < length; i++) {
			int code = (int) (Math.random() * 10);
			codeStr += code;
		}
		return codeStr;
	}
}
