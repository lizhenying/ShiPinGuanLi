package com.jnmd.video.controller;


import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jnmd.video.pojo.Admin;
import com.jnmd.video.service.AdminService;
import com.jnmd.video.utils.Md5Utils;

@Controller
@RequestMapping("admin")
public class LoginController {
	
	@Autowired
	private AdminService adminService;
	/*
	 * 登录页面
	 */
	@RequestMapping("showLogin")
	public String showLogin(HttpSession session){
		return "admin/login/showLogin";
	}
	
	/*
	 * 登录操作
	 */
	@RequestMapping("login")
	public String login(String username,String password,HttpSession session){
		System.out.println("-----------------login------------------");
		//密码 md5加密
		String pass = Md5Utils.md5(password);
		Admin admin = new Admin();
		admin.setUsername(username);
		admin.setPassword(pass);
		int result = adminService.findAdmin(admin);
		if(result > 0){
			session.setAttribute("username", username);
			return "admin/login/videoManageHomepage";
		}else{
			System.out.println("redirect showLogin");
			return "redirect:showLogin";
		}
	}
	
	//loginOut.action
	@RequestMapping("loginOut")
	public String loginOut(HttpSession session){
		session.removeAttribute("username");
		return "admin/login/showLogin";
	}
	
	@RequestMapping("videoManageHomepage")
	public String homepage(){
		return "admin/login/videoManageHomepage";
	}
}
