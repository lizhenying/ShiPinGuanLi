package com.jnmd.video.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jnmd.video.pojo.Course;
import com.jnmd.video.pojo.Speaker;
import com.jnmd.video.pojo.Video;
import com.jnmd.video.service.FrontCourseService;
import com.jnmd.video.service.FrontSpeakerService;
import com.jnmd.video.service.FrontVideoService;

@Controller
@RequestMapping("video")
public class FrontVideoController {
	
	@Autowired
	private FrontVideoService frontVideoService;
	
	@Autowired
	private FrontSpeakerService frontSpeakerService;
	
	@Autowired
	private FrontCourseService frontCourseService;
	
	@RequestMapping("playVideo")
	public String showPlayVideo(int videoId,String subjectName){
		Video video = frontVideoService.findVideoById(videoId);
		Speaker speaker = frontSpeakerService.findSpeakerById(video.getSpearkerId());
		Course course = frontCourseService.findCourseByCourseId(video.getCourseId());
		
		
		return "before/front/video/index";
	}
}
