package com.jnmd.video.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jnmd.video.pojo.Course;
import com.jnmd.video.service.CourseService;

@Controller
@RequestMapping("course")
public class CourseController {
	
	@Autowired
	private CourseService courseService;
	
	/*
	 * 课程管理页面
	 */
	@RequestMapping("showCourse")
	public String showCourse(Model model){
		List<Course> course = courseService.findCourseAll();
		model.addAttribute("courseList", course);
		return "admin/course/showCourse";
	}
	
	/*
	 * 添加课程 或者 编辑课程信息
	 */
	@RequestMapping("addOrEditCourse")
	public String addCourse(int id){
		if(id == 0){
			courseService.addCourse();
			return "admin/course/addCourse";
		}else{
			courseService.findCourseById();
			return "admin/course/addCourse";
		}
	}
	
	/*
	 * 删除课程
	 */
	@RequestMapping("deleteCourse")
	@ResponseBody
	public String deleteCourse(int id){
		courseService.deleteCourse(id);
		return "success";
	}
}
