package com.jnmd.video.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jnmd.video.pojo.Course;
import com.jnmd.video.pojo.Speaker;
import com.jnmd.video.service.CourseService;
import com.jnmd.video.service.SpeakerService;



@Controller
@RequestMapping("speaker")
public class SpeakerController {
	
	@Autowired
	private SpeakerService speakerService;
	
	@Autowired
	private CourseService courseService;
	/*
	 * 展示主讲人
	 */
	@RequestMapping("showSpeaker")
	public String showSpeaker(Model model){
		List<Speaker> speakerList = speakerService.findSpeakerAll();
		model.addAttribute("speakerList", speakerList);
		return "admin/speaker/showSpeaker";
	}
	/*
	 * 添加主讲人
	 */
	@RequestMapping("addOrEditSpeaker/{id}")
	public String addMainTeacher(@PathVariable int id,Model model){
		if(id == 0){
			//添加
			return "admin/speaker/addSpeaker";
		}else{
			//编辑
			System.out.println(id);
			Speaker speaker = speakerService.findSpeakerById(id);
			List<Speaker> speakerList = speakerService.findSpeakerAll();
			List<Course> courseList = courseService.findCourseAll();
			model.addAttribute("speaker", speaker);
			model.addAttribute("speakerList", speakerList);
			model.addAttribute("courseList", courseList);
			return "admin/speaker/editSpeaker";
		}
	}
	
	/*
	 * 删除
	 */
	@RequestMapping("deleteSpeaker")
	@ResponseBody
	public String deleteSpeakerById(int id){
		speakerService.deleteSpeakerById(id);
		return "success";
	}
	
	
	/*
	 * 保存
	 */
	@RequestMapping("saveOrUpdateSpeaker")
	public String saveOrUpdateSpeaker(Speaker speaker,Model model){
		//编辑
		if(speaker.getId() != null && speaker.getId() != 0){
			speakerService.updateSpeaker(speaker);
		}else{
			//添加
			speakerService.addSpeaker(speaker);
		}
		return "redirect:showSpeaker";
	}
	
}
