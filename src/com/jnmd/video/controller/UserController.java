package com.jnmd.video.controller;

import java.io.File;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.jnmd.video.pojo.User;
import com.jnmd.video.service.UserService;
import com.jnmd.video.utils.ImageCut;
import com.jnmd.video.utils.JavaEmailSender;
import com.jnmd.video.utils.Md5Utils;
import com.jnmd.video.utils.RandomCode;

@Controller
public class UserController {

	@Autowired
	private UserService userService;
	
	//验证 注册邮箱
	@RequestMapping("user/validateEmail")
	@ResponseBody
	public String validateEmail(String email){
		int count = userService.getCountByEmail(email);
		//该邮箱可用
		//if(count == 0){
		//	return "success";
		//}
		//该邮箱已被注册 不可用
		//return "fail";
		return count == 0? "success": "fail";
	}
	
	@RequestMapping("user/insertUser")
	@ResponseBody
	public String addUser(User user,HttpSession session){
		session.setAttribute("_userAccount", user.getEmail());
		user.setPassword(Md5Utils.md5(user.getPassword()));
		int result = userService.addUser(user);
		if(result > 0){
			return "success";
		}
		return "fail";
	}
	//退出
	@RequestMapping("user/logout")
	public String logout(HttpSession session){
		session.removeAttribute("_userAccount");
		return "redirect:/index";
	}
	
	//验证登录时邮箱 合法
	@RequestMapping("user/validateLoginEmail")
	@ResponseBody
	public String validateLoginEmail(String email){
		int count = userService.getCountByEmail(email);
		return count > 0? "success": "fail";
	}
	
	//验证 登录时  邮箱密码合法
	@RequestMapping("user/validateLogin")
	public String validataLogin(User user,HttpSession session){
		System.out.println(user.getEmail() +"-----"+user.getPassword());
		if(null != user.getPassword() && "" != user.getPassword()){
			user.setPassword(Md5Utils.md5(user.getPassword()));
		}
		int count = userService.getCountByUser(user);
		if(count > 0){
			session.setAttribute("_userAccount", user.getEmail());
			return "success";
		}else{
			return "fail";
		}
	}
	
	//忘记密码
	@RequestMapping("user/forgetPassword")
	public String userForgetPwd(){
		return "before/front/user/forget_pwd";
	}
	
	//发送邮件
	@RequestMapping("user/sendEmail")
	@ResponseBody
	public String sendEmail(String email){
		String code = RandomCode.getRandomCode(6);
		try {
			JavaEmailSender.sendEmail(email, "小霸王学习网", "您的验证码为"+code+"有效时间24小时");
			//把随机码保存在数据库中，   用于校验用户输入
			userService.saveRandomCode(email,code);
			return "success";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "fail";
		}
	}
	
	//校验用户输入的验证码是否正确
	@RequestMapping("user/validateEmailCode")
	public String validateEmailCode(String email,String code,HttpServletRequest req){
		User user = userService.getUserByAccount(email);
		if(user.getCode().equals(code)){
			return "/before/front/user/reset_pwd";
		}else{
			req.setAttribute("errorMsg", "验证码有误");
			return "before/front/user/forget_pwd";
		}
	}
	
	//重置密码
	
	
	
	
	//user/index 用户个人中心
	@RequestMapping("user/index")
	public String showCenter(HttpSession session,Model model){
		String email = (String) session.getAttribute("_userAccount");
		User user = userService.getUserByAccount(email);
		model.addAttribute("user", user);
		return "/before/front/user/index";
	}
	
	
	//user/profile 更改资料
	@RequestMapping("user/profile")
	public String updateProfile(HttpSession session,Model model){
		String email = (String) session.getAttribute("_userAccount");
		User user = userService.getUserByAccount(email);
		model.addAttribute("user", user);
		return "/before/front/user/profile";
	}
	// 保存更改的资料
	@RequestMapping("updateUser")
	public String updateUser(User user,Model model){
		System.out.println("保存更改的资料");
		userService.updateUser(user);
		return "redirect:user/index";
	}
	
	//更改头像
	@RequestMapping("user/avatar")
	public String updateImg(HttpSession session,Model model){
		String email = (String) session.getAttribute("_userAccount");
		User user = userService.getUserByAccount(email);
		model.addAttribute("user", user);
		return "/before/front/user/avatar";
	}
	
	@RequestMapping("user/upLoadImage")
	public String upLoadImage(MultipartFile image_file,HttpSession session,HttpServletRequest req,Model model){
		String fileName = image_file.getOriginalFilename();
		String randomUUID = UUID.randomUUID().toString();
		String extName = fileName.substring(fileName.lastIndexOf("."));
		String newName = randomUUID + extName;
		float x1=0,x2=0,y1=0,y2=0;
		boolean isCut = false;
		if(null != req.getParameter("x1") && "" != req.getParameter("x1")){
			x1 = Float.parseFloat(req.getParameter("x1"));
			x2 = Float.parseFloat(req.getParameter("x2"));
			y1 = Float.parseFloat(req.getParameter("y1"));
			y2 = Float.parseFloat(req.getParameter("y2"));
			isCut = true;
		}
			try {
				image_file.transferTo(new File("c://img/"+newName));
				String email = (String) session.getAttribute("_userAccount");
				userService.updateUserImg(email,newName);
				
				if(isCut){
					ImageCut imgCut = new ImageCut();
					imgCut.cutImage("c://img/"+newName, (int)x1, (int)y1,(int)(x2-x1),(int)(y2-y1));
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return "redirect:avatar";
	}
	
	
	//密码安全
	@RequestMapping("user/password")
	public String pwdSafe(){
		
		return "/before/front/user/password";
	}
	
}
