package com.jnmd.video.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jnmd.video.pojo.Course;
import com.jnmd.video.pojo.Subject;
import com.jnmd.video.pojo.Video;
import com.jnmd.video.service.FrontCourseService;
import com.jnmd.video.service.FrontSubjectService;
import com.jnmd.video.service.FrontVideoService;

@Controller
@RequestMapping("front/course")
public class FrontCourseController {
	
	@Autowired
	private FrontSubjectService frontsubjectService;
	
	@Autowired
	private FrontCourseService frontCourseService;
	
	@Autowired
	private FrontVideoService frontVideoService;
	
	@RequestMapping("index")
	public String showSubjectById(int subjectId,Model model){
		System.out.println(subjectId);
		Subject subject = frontsubjectService.findSubjectById(subjectId);
		List<Course> courseList = frontCourseService.findCourseBySubjectId(subjectId);
		System.out.println(courseList);
		if(courseList.size() != 0){
			subject.setCourseList(courseList);
		}
		for (Course course : courseList) {
			System.out.println("course....");
			System.out.println(course.getId());
			List<Video> videoList = frontVideoService.findVideoByCourseId(course.getId());
			System.out.println(videoList);
			if(videoList.size() != 0){
				course.setVideoList(videoList);
			}
		}
		System.out.println("subject..");
		model.addAttribute("subject", subject);
		return "before/front/course/index";
	}
}
