package com.jnmd.video.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jnmd.video.pojo.Video;
import com.jnmd.video.pojo.VideoQueryVo;
import com.jnmd.video.service.CourseService;
import com.jnmd.video.service.SpeakerService;
import com.jnmd.video.service.VideoService;
import com.jnmd.video.utils.Page;
import com.jnmd.video.utils.TimeUtils;

@Controller
@RequestMapping("/video")
public class VideoController {
	
	@Autowired
	private VideoService videoService;
	
	@Autowired
	private SpeakerService speakerService;
	
	@Autowired
	private CourseService courseService;
	
	@RequestMapping("showVideo")
	public ModelAndView showVideoList(VideoQueryVo videoQueryVo){
		
		ModelAndView modelAndView =new ModelAndView();
		System.out.println("进入方法："+videoQueryVo.toString());
		
		videoQueryVo.setBegin((videoQueryVo.getPage() - 1) * videoQueryVo.getSize());
		List<Video> list = videoService.findVideoByVo(videoQueryVo);
		//将时间格式化
		for (Video video2 : list) {
			if(null != video2.getTime() && video2.getTime() != 0){
				String formatTime = TimeUtils.getFormatTime(video2.getTime());
				video2.setShowTime(formatTime);
			}
		}
		
		Page<Video> page = new Page<Video>();
		page.setPage(videoQueryVo.getPage());
		page.setSize(videoQueryVo.getSize());
		
		page.setTotal(videoService.getCountVideo(videoQueryVo));
		page.setRows(list); //分页查询的结果集
//		modelAndView.addObject("list",list );
		modelAndView.addObject("speakerList", speakerService.findSpeakerAll());
		modelAndView.addObject("courseList", courseService.findCourseAll());
		modelAndView.addObject("page", page);
		
		
		modelAndView.setViewName("admin/video/showVideo");
		return modelAndView;
	}
	
	/*
	 *  添加视频或者编辑视频信息
	 */
	@RequestMapping("aoeVideo")
	public String editVideo(int id,Model model){
		if(id == 0){
			//添加视频
			model.addAttribute("speakerList", speakerService.findSpeakerAll());
			model.addAttribute("courseList", courseService.findCourseAll());
			return "admin/video/addVideo";
		}else{
			//编辑视频
			model.addAttribute("speakerList", speakerService.findSpeakerAll());
			model.addAttribute("courseList", courseService.findCourseAll());
			Video video = videoService.findVideoById(id);
			model.addAttribute("video", video);
			return "admin/video/editVideoInfo";
		}
	}
	/*
	 * 删除video
	 */
	@RequestMapping("deleteVideo")
	@ResponseBody
	public String deleteVideo(int id){
		videoService.deleteVideo(id);
		return "success";
	}
	
	/*
	 * 保存
	 */
	@RequestMapping("addOrEditVideo")
	public String addOrEditVideo(Video video){
		System.out.println(video.getId());
		//修改
		if(null != video.getId() && video.getId()!=0){
			videoService.updateVideo(video);
		//新增
		}else{
			videoService.addVideo(video);
		}
		return "redirect:showVideo";
	}
	
	
	@RequestMapping("batchDelete")
	public String batcheDeleteVideo(Integer[] ids){
		videoService.batchDelete(ids);
		return "redirect:showVideo";
		
	}
}
