package com.jnmd.video.pojo;

public class VideoQueryVo {
	private String title;
	private int spearkerId;
	private String speakerName;
	private int courseId;
	private String courseTitle;
	
	private int page = 1;//当前页
	private int size = 5;//一页多少条
	private int begin = 0;//开始位置
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getSpearkerId() {
		return spearkerId;
	}
	public void setSpearkerId(int spearkerId) {
		this.spearkerId = spearkerId;
	}
	public int getCourseId() {
		return courseId;
	}
	public void setCourseId(int courseId) {
		this.courseId = courseId;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	public int getBegin() {
		return begin;
	}
	public void setBegin(int begin) {
		this.begin = begin;
	}
	public String getSpeakerName() {
		return speakerName;
	}
	public void setSpeakerName(String speakerName) {
		this.speakerName = speakerName;
	}
	public String getCourseTitle() {
		return courseTitle;
	}
	public void setCourseTitle(String courseTitle) {
		this.courseTitle = courseTitle;
	}
	@Override
	public String toString() {
		return "VideoQueryVo [title=" + title + ", spearkerId=" + spearkerId
				+ ", speakerName=" + speakerName + ", courseId=" + courseId
				+ ", courseTitle=" + courseTitle + ", page=" + page + ", size="
				+ size + ", begin=" + begin + "]";
	}
	
}
