package com.jnmd.video.mapper;

import java.util.List;

import com.jnmd.video.pojo.Video;

public interface FrontVideoMapper {

	List<Video> selectVideoByCourseId(Integer id);

	Video selectVideoById(int videoId);

}
