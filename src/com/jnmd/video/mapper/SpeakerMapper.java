package com.jnmd.video.mapper;

import java.util.List;

import com.jnmd.video.pojo.Speaker;

public interface SpeakerMapper {

	
	public List<Speaker> selectAllSpeaker();

	public Speaker selectSpeakerById(int id);

	public void updateSpeaker(Speaker speaker);

	public void insertIntoSpeaker(Speaker speaker);

	public void deleteSpeakerById(int id);
}
