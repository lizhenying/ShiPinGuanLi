package com.jnmd.video.mapper;

import java.util.List;

import com.jnmd.video.pojo.Course;

public interface CourseMapper {
	public List<Course> selectAllCourse();
}
