package com.jnmd.video.mapper;

import com.jnmd.video.pojo.Subject;

public interface FrontSubjectMapper {

	Subject selectSubjectById(int subjectId);

}
