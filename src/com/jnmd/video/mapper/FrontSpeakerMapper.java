package com.jnmd.video.mapper;

import com.jnmd.video.pojo.Speaker;

public interface FrontSpeakerMapper {

	Speaker selectSpeakerById(Integer spearkerId);

}
