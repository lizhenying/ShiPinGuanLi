package com.jnmd.video.mapper;

import com.jnmd.video.pojo.Admin;

public interface AdminMapper {

	int selectAdmin(Admin admin);

}
