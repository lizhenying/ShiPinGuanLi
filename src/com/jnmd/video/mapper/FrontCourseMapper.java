package com.jnmd.video.mapper;

import java.util.List;

import com.jnmd.video.pojo.Course;

public interface FrontCourseMapper {

	List<Course> selectCourseBySubjectId(int subjectId);

	Course selectCourseById(Integer courseId);

}
