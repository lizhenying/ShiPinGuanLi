package com.jnmd.video.mapper;

import java.util.List;

import com.jnmd.video.pojo.BatchDeleteVo;
import com.jnmd.video.pojo.Video;
import com.jnmd.video.pojo.VideoQueryVo;

public interface VideoMapper {

	Video selectVideoById(int id);

	void updateVideo(Video video);
	
	void insertIntoVideo(Video video);

	void deleteVideo(int id);

	List<Video> selectVideoByVo(VideoQueryVo video);

	int selectVideoAll(VideoQueryVo videoQueryVo);

	void batchDeleteByVo(BatchDeleteVo bacthDeleteVo);
}
