package com.jnmd.video.service;

import com.jnmd.video.pojo.Admin;

public interface AdminService {

	int findAdmin(Admin admin);

}
