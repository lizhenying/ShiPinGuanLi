package com.jnmd.video.service;

import java.util.List;

import com.jnmd.video.pojo.Course;

public interface FrontCourseService {

	List<Course> findCourseBySubjectId(int subjectId);

	Course findCourseByCourseId(Integer courseId);

}
