package com.jnmd.video.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jnmd.video.mapper.FrontSpeakerMapper;
import com.jnmd.video.pojo.Speaker;
import com.jnmd.video.service.FrontSpeakerService;

@Service
public class FrontSpeakerServiceImpl implements FrontSpeakerService {

	@Autowired
	private FrontSpeakerMapper frontSpeakerMapper;
	
	@Override
	public Speaker findSpeakerById(Integer spearkerId) {
		// TODO Auto-generated method stub
		return frontSpeakerMapper.selectSpeakerById(spearkerId);
	}

}
