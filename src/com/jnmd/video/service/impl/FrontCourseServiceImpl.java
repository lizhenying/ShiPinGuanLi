package com.jnmd.video.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jnmd.video.mapper.FrontCourseMapper;
import com.jnmd.video.pojo.Course;
import com.jnmd.video.service.FrontCourseService;

@Service
public class FrontCourseServiceImpl implements FrontCourseService {

	@Autowired
	private FrontCourseMapper fontCourseMapper;
	
	@Override
	public List<Course> findCourseBySubjectId(int subjectId) {
		// TODO Auto-generated method stub
		return fontCourseMapper.selectCourseBySubjectId(subjectId);
	}

	@Override
	public Course findCourseByCourseId(Integer courseId) {
		// TODO Auto-generated method stub
		return fontCourseMapper.selectCourseById(courseId);
	}

}
