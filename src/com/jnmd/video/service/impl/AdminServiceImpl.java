package com.jnmd.video.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jnmd.video.mapper.AdminMapper;
import com.jnmd.video.pojo.Admin;
import com.jnmd.video.service.AdminService;

@Service
public class AdminServiceImpl implements AdminService {

	@Autowired
	private AdminMapper adminMapper;
	
	@Override
	public int findAdmin(Admin admin) {
		// TODO Auto-generated method stub
		return adminMapper.selectAdmin(admin);
	}

}
