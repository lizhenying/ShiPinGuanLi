package com.jnmd.video.service.impl;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jnmd.video.mapper.VideoMapper;
import com.jnmd.video.pojo.BatchDeleteVo;
import com.jnmd.video.pojo.Video;
import com.jnmd.video.pojo.VideoQueryVo;
import com.jnmd.video.service.VideoService;

@Service
public class VideoServiceImpl implements VideoService {
	
	@Autowired
	private VideoMapper videoMapper;

	@Override
	public void updateVideo(Video video) {
		// TODO Auto-generated method stub
		videoMapper.updateVideo(video);
	}
	
	
	@Override
	public Video findVideoById(int id) {
		// TODO Auto-generated method stub
		return videoMapper.selectVideoById(id);
	}

	@Override
	public void addVideo(Video video) {
		// TODO Auto-generated method stub
		videoMapper.insertIntoVideo(video);
	}

	@Override
	public void deleteVideo(int id) {
		// TODO Auto-generated method stub
		videoMapper.deleteVideo(id);
	}

	@Override
	public List<Video> findVideoByVo(VideoQueryVo video) {
		// TODO Auto-generated method stub
		return videoMapper.selectVideoByVo(video);
	}

	@Override
	public int getCountVideo(VideoQueryVo videoQueryVo) {
		// TODO Auto-generated method stub
		return videoMapper.selectVideoAll(videoQueryVo);
	}


	@Override
	public void batchDelete(Integer[] ids) {
		// TODO Auto-generated method stub
		List<Integer> asList = Arrays.asList(ids);
		BatchDeleteVo bacthDeleteVo = new BatchDeleteVo();
		bacthDeleteVo.setIds(asList);
		videoMapper.batchDeleteByVo(bacthDeleteVo);
	}

}
