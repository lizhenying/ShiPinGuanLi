package com.jnmd.video.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jnmd.video.mapper.FrontVideoMapper;
import com.jnmd.video.pojo.Video;
import com.jnmd.video.service.FrontVideoService;

@Service
public class FrontVideoServiceImpl implements FrontVideoService {

	@Autowired
	private FrontVideoMapper frontVideoMapper;
	
	@Override
	public List<Video> findVideoByCourseId(Integer id) {
		// TODO Auto-generated method stub
		return frontVideoMapper.selectVideoByCourseId(id);
	}

	@Override
	public Video findVideoById(int videoId) {
		// TODO Auto-generated method stub
		return frontVideoMapper.selectVideoById(videoId);
	}

}
