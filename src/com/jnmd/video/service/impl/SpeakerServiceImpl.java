package com.jnmd.video.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jnmd.video.mapper.SpeakerMapper;
import com.jnmd.video.pojo.Speaker;
import com.jnmd.video.service.SpeakerService;

@Service
public class SpeakerServiceImpl implements SpeakerService {
	
	@Autowired
	private SpeakerMapper speakerMapper;

	

	@Override
	public List<Speaker> findSpeakerAll() {
		// TODO Auto-generated method stub
		return speakerMapper.selectAllSpeaker();
	}



	@Override
	public Speaker findSpeakerById(int id) {
		// TODO Auto-generated method stub
		return speakerMapper.selectSpeakerById(id);
	}



	@Override
	public void updateSpeaker(Speaker speaker) {
		// TODO Auto-generated method stub
		speakerMapper.updateSpeaker(speaker);
	}



	@Override
	public void addSpeaker(Speaker speaker) {
		// TODO Auto-generated method stub
		speakerMapper.insertIntoSpeaker(speaker);
	}



	@Override
	public void deleteSpeakerById(int id) {
		// TODO Auto-generated method stub
		speakerMapper.deleteSpeakerById(id);
	}

}
