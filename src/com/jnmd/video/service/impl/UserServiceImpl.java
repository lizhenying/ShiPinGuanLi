package com.jnmd.video.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jnmd.video.mapper.UserMapper;
import com.jnmd.video.pojo.User;
import com.jnmd.video.pojo.UserExample;
import com.jnmd.video.pojo.UserExample.Criteria;
import com.jnmd.video.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserMapper userMapper;
	
	@Override
	public int getCountByEmail(String email) {
		// TODO Auto-generated method stub
		UserExample example = new UserExample();
		Criteria criteria = example.createCriteria();
		criteria.andEmailEqualTo(email);
		return userMapper.countByExample(example);
	}

	@Override
	public int addUser(User user) {
		// TODO Auto-generated method stub
		return userMapper.insert(user);
	}

	@Override
	public int getCountByUser(User user) {
		// TODO Auto-generated method stub
		UserExample example = new UserExample();
		Criteria criteria = example.createCriteria();
		criteria.andEmailEqualTo(user.getEmail());
		criteria.andPasswordEqualTo(user.getPassword());
		return userMapper.countByExample(example);
	}

	@Override
	public User getUserByAccount(String email) {
		// TODO Auto-generated method stub
		UserExample example = new UserExample();
		Criteria criteria = example.createCriteria();
		criteria.andEmailEqualTo(email);
		User user = userMapper.selectByExample(example).get(0);
		return user;
	}

	@Override
	public void updateUser(User user) {
		// TODO Auto-generated method stub
		UserExample example = new UserExample();
		Criteria criteria = example.createCriteria();
		criteria.andIdEqualTo(user.getId());
		userMapper.updateByExampleSelective(user, example);
	}

	@Override
	public void saveRandomCode(String email, String code) {
		// TODO Auto-generated method stub
		UserExample example = new UserExample();
		Criteria criteria = example.createCriteria();
		criteria.andEmailEqualTo(email);
		User user = new User();
		user.setCode(code);
		userMapper.updateByExampleSelective(user, example);
	}

	@Override
	public void updateUserImg(String email, String newName) {
		// TODO Auto-generated method stub
		UserExample example = new UserExample();
		Criteria criteria = example.createCriteria();
		criteria.andEmailEqualTo(email);
		User user = new User();
		user.setImgurl(newName);
		userMapper.updateByExampleSelective(user, example);
	}

}
