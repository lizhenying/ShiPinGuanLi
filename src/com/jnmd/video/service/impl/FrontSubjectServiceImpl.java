package com.jnmd.video.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jnmd.video.mapper.FrontSubjectMapper;
import com.jnmd.video.pojo.Subject;
import com.jnmd.video.service.FrontSubjectService;

@Service
public class FrontSubjectServiceImpl implements FrontSubjectService{

	@Autowired
	private FrontSubjectMapper frontSubjectMapper;
	/*
	 * (non-Javadoc)
	 * @see com.jnmd.video.service.FrontSubjectService#findSubjectById(int)
	 * 通过id查找subject
	 */
	@Override
	public Subject findSubjectById(int subjectId) {
		// TODO Auto-generated method stub
		return frontSubjectMapper.selectSubjectById(subjectId);
	}

}
