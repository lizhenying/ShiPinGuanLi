package com.jnmd.video.service;

import java.util.List;

import com.jnmd.video.pojo.Course;

public interface CourseService {
	
	public List<Course> findCourseAll();

	public void addCourse();

	public void findCourseById();

	public void deleteCourse(int id);

}
