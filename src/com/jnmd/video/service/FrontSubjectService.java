package com.jnmd.video.service;

import com.jnmd.video.pojo.Subject;

public interface FrontSubjectService {

	Subject findSubjectById(int subjectId);

}
