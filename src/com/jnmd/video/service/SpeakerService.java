package com.jnmd.video.service;

import java.util.List;

import com.jnmd.video.pojo.Speaker;

public interface SpeakerService {

	public List<Speaker> findSpeakerAll();

	public Speaker findSpeakerById(int id);

	public void updateSpeaker(Speaker speaker);

	public void addSpeaker(Speaker speaker);

	public void deleteSpeakerById(int id);
}
