package com.jnmd.video.service;

import com.jnmd.video.pojo.User;

public interface UserService {

	int getCountByEmail(String email);

	int addUser(User user);

	int getCountByUser(User user);

	User getUserByAccount(String email);

	void updateUser(User user);

	void saveRandomCode(String email, String code);

	void updateUserImg(String email, String newName);

}
