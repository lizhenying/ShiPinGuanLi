package com.jnmd.video.service;

import java.util.List;

import com.jnmd.video.pojo.Video;
import com.jnmd.video.pojo.VideoQueryVo;

public interface VideoService {

	public Video findVideoById(int id);

	public void updateVideo(Video video);

	public void addVideo(Video video);

	public void deleteVideo(int id);

	public List<Video> findVideoByVo(VideoQueryVo videoQueryVo);

	public int getCountVideo(VideoQueryVo videoQueryVo);

	public void batchDelete(Integer[] ids);
}
