package com.jnmd.video.service;

import java.util.List;

import com.jnmd.video.pojo.Video;

public interface FrontVideoService {

	List<Video> findVideoByCourseId(Integer id);

	Video findVideoById(int videoId);

}
